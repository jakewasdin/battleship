client sent messages:
setup:	
	info: name, number of players
	description: creates a game with the given number of players
	responses: inTheGame

Join:	
	info: name
	description: look for an already started game to play in
	responses: inTheGame

boom:	
	info: index of spot (as one dimensional array)
	description: firing a shot at the location (client limited to every 2 seconds - no turns)
	responses: boomEcho

readTheNews: 
	info: empty
	description: periodically check (1 sec - client side timer) the status of the game
	responses: fight, end, badNews, taps


server sent messages (replies):
inTheGame: 
	info: array[shipstart index, shipstop index]
	description: the client is in the game and is sent their ships locations for this game
	responds to: Setup/Join
	
fight:
	info: empty
	description: the game has begun and the clients can start firing
	responds to: readTheNews

boomEcho: 
	info: index of spot, bool hitOrMiss
	description: whether or not the shot at the location was a hit or a miss
	responds to: boom

badNews:
	info: array[index of spot]
	description: your ship was hit at the location
	responds to: readTheNews

taps:
	info: empty
	description: all your ships have been sunk. Play sad music (client side music - maybe)
	responds to: readTheNews

end:	
	info: array[usernames and scores in their ranking]
	description: The win conditions have been met
	responds to: readTheNews


