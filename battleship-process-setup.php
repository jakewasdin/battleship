<?php
	include 'session.php';

	$isGameGoing = $_SESSION["gameGoing"];
	if(!$isGameGoing)
	{
		if(!$_SESSION["gameInitiating"])
		{
			$_SESSION["gameInitiating"] = TRUE;
			$_SESSION["board"] = array_fill(0, 143, "water");
			$_SESSION["numPlayers"] = $_POST["players"];
			$_SESSION["playersAlive"] = array_fill(0, $_POST["players"], TRUE);
			$_SESSION["used_elements"] = Array();
			$_SESSION["shipsLocation"] = Array();

			$players = Array();
			array_push($players, str_replace(' ', '',$_POST["name"]));
			$_SESSION["players"] = $players;
	
			$shipsHit = array();
			array_push($shipsHit, array_fill(0, 5, FALSE));
			array_push($shipsHit, array_fill(0, 4, FALSE));
			array_push($shipsHit, array_fill(0, 3, FALSE));
			array_push($shipsHit, array_fill(0, 3, FALSE));
			array_push($shipsHit, array_fill(0, 2, FALSE));
		
			$arrayOfPlayers = array();
			array_push($arrayOfPlayers, $shipsHit);
			$_SESSION["shipsStatus"] = $arrayOfPlayers;

			$playersAlive = array();
			array_push($playersAlive , TRUE);
			$_SESSION["playersAlive"] = $playersAlive;

			$_SESSION["scores"] = array_fill(0, $_POST["players"], 0);

			//echo the userID
			echo "0 ";

			//generate the ship
			include 'static_ship_generation.php';	
		}	
		else
		{
			echo "initing";
		}		
	}
	else
	{
		echo "going";
	}
?>