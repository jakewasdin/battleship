// global variables
var hasStarted = false;
var canShoot = false;
var allShipsAreSunk = false;
var boardWidth = 12;
var boardHeight = 12;
var userID = -1;

//creates or joins the existing game and then creates the board
//inTheGame: 	itg cell# cell# .... cell# (where each cell where a ship is at is included seperated by a space)
function createGame()
{	
	//we have to save these of first before they are destroyed creating the board
	var playername = document.getElementById("playerName").value;
	var numplayers = document.getElementById("desiredPlayers").value;

	//send the request and tell it how to process it
	xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = joinGameHelper;

	xmlhttp.open("POST", "battleship-process-setup.php");
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.send("name=" + playername + "&players=" + numplayers);
}

function joinGame()
{
	//we have to save these of first before they are destroyed creating the board
	var playername = document.getElementById("playerName").value;

	//send the request and tell it how to process it
	xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = joinGameHelper;

	xmlhttp.open("POST", "battleship-process-join.php");
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.send("name=" + playername);
}

//handles the response from the server from trying to join or create a game and initializes the game board and loads the ship
function joinGameHelper()
{
	if (xmlhttp.readyState === 4) 
	{
	        if (xmlhttp.status == 200 && xmlhttp.status < 300) 
		{
			//split the passed back values on a space
			var parts = xmlhttp.responseText.split(" ");
			if(parts[0] == "initing")
			{
				var status = document.getElementById('statusdiv');
				status.innerHTML = "<p>A Game has already been created and is waiting for people to join</p>";
			}
			else if(parts[0] == "none")
			{
				var status = document.getElementById('statusdiv');
				status.innerHTML = "<p>No game has yet been created</p>";
			}
			else if(parts[0] == "going")
			{
				var status = document.getElementById('statusdiv');
				status.innerHTML = "<p>A Game is currently in progress and cannot be joined or a new one created</p>";
			}
			else
			{
				//get our id and go
				userID = parts[0];
				parts.splice(0,1);

				//generate the field now
				initializeBoard();

				//load our ships
				loadShips(parts);
			}
		}
	}
}

//creates the playing board and timer
function initializeBoard()
{
	//make the timer and the start of the table
	var newHtml = "<div id=\"timer\"><p>Waiting To Start Game!</p></div><table id=\"board\">";

	for(var i = 0; i < boardHeight; i++)
	{
		newHtml += "<tr>";
		for(var j = 0; j < boardWidth; j++)
		{
			newHtml += "<td id=\"" + (i*boardWidth + j) + "\" class=\"water\" onClick=\"boom(this);\">" + (i*12 + j) + "</td>";
		}
		newHtml += "</tr>";
	}
	//make the end of the table and stick it in the body
	newHtml += "</table>";
	document.body.innerHTML = newHtml;

							
	//set up the update the news timer
	setInterval(readTheNews, 500);
}

//loads the ships onto the grid
function loadShips(shipLocations)
{
	for(var i = 0; i < shipLocations.length; i++)
	{
		var cellToMakeShip = document.getElementById(shipLocations[i]);
		cellToMakeShip.setAttribute("class", "ship");
	}
}

//fires a shot at the given cell
function boom(cell)
{
	if(canShoot && !allShipsAreSunk)
	{
		if(cell.getAttribute("class") == "water" || cell.getAttribute("class") == "hit" || 
			cell.getAttribute("class") == "miss" || cell.getAttribute("class") == "processing")
		{
			cell.setAttribute("class", "processing");

			//sends the boom and sets the processing function
			xmlhttp = new XMLHttpRequest();

			xmlhttp.onreadystatechange = processBoom;

			xmlhttp.open("POST", "battleship-process-boom.php");
                        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                        xmlhttp.send("id=" + userID + "&boomLocation=" + cell.id);

			falseCanShoot();
		}	
	}
}

//assumes responseText is in the form "(t/f) cell#" i.e. "t 32"
function processBoom()
{
	if (xmlhttp.readyState === 4) 
	{
	        if (xmlhttp.status == 200 && xmlhttp.status < 300) 
		{
			//split the passed back values on a space
			var parts = xmlhttp.responseText.split(" ");
			
			if(parts[0] == "taps")
			{
				scuttleAllShips();
				var cellRespondedTo = document.getElementById(parts[1]);
				cellRespondedTo.setAttribute("class", "water");
			}
			else
			{
				//get the cell
				var cellRespondedTo = document.getElementById(parts[1]);

				if(parts[0] == "t")
				{
					cellRespondedTo.setAttribute("class", "hit");
				}
				else
				{
					cellRespondedTo.setAttribute("class", "miss");
				}
			}
		}
	}
}


//checkd for any gamme updates - started, in the game, game over or ships been hit or all are sunk
function readTheNews()
{
	//send the request and tell it how to process it
	xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = readBetweenTheLines;

	xmlhttp.open("POST", "battleship-process-readTheNews.php");
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        xmlhttp.send("id=" + userID + "&clientStarted=" + hasStarted); //we dont need to send any info - maybe a player ID later?

}

//Note: all the messages are mutually exclusive - we only should need to respond with one ever
//this assumes each of the protocols are in this string format
//fight:	fight
//badNews:	bn cell# cell# ... cell# (where each cell that has been hit since the last update is seperated by a space)
//end:		gg userame1 score1 username2 score2 .... usernamen scoren (where each user is sent with their score in the next space in order of score)
function readBetweenTheLines()
{
	if (xmlhttp.readyState === 4) 
	{
	        if (xmlhttp.status == 200 && xmlhttp.status < 300) 
		{
			//split the passed back values on a space
			var parts = xmlhttp.responseText.split(" ");
			
			//do stuff based on the first part
			if(parts[0] == "wait")
			{
				//do nothing - maybe have a timer just cause?
			}
			else if(parts[0] == "fight") {
				startGame();
			}
			else if(parts[0] == "bn") {
				damageShips(parts);
			}
			else if(parts[0] == "taps") {
				scuttleAllShips();
			}
			else if(parts[0] == "gg") {
				gameOver(parts);
			}
		}
	}
}

function startGame()
{
	hasStarted = true;

	//set the shot timer 
	setInterval(trueCanShoot, 1500);
	
	//let them start guns ablazin!
	trueCanShoot();
}


//sets that the user has fired
function falseCanShoot() 
{
	canShoot = false;
	var timer = document.getElementById('timer');
	timer.innerHTML = "<p>Reloading</p>";
}



//sets that the user has fired
function trueCanShoot() 
{
	if(!allShipsAreSunk)
	{
		canShoot = true;
		var timer = document.getElementById('timer');
		timer.innerHTML = "<p class='ready'>Ready to Shoot!</p>";
	}
}

function damageShips(shipsHit)
{
	for(var i = 1; i < shipsHit.length; i++)
	{
		if(shipsHit[i] != 9001)
		{
			var cellHit = document.getElementById(shipsHit[i]);
			cellHit.setAttribute("class", "hit-ship");
		}
	}
}

function scuttleAllShips()
{
	for(var i = 0; i < boardWidth * boardHeight; i++)
	{
		var cellToScuttle = document.getElementById(i);
		if(cellToScuttle.getAttribute("class") == "ship")
		{
			cellToScuttle.setAttribute("class", "hit-ship");
		}
	}
	allShipsAreSunk = true;

	var timer = document.getElementById('timer');
	timer.innerHTML = "<p>All Your Ships <br />are sleeping with <br/>the fishies! :(</p>";
}

function gameOver(playersAndScores)
{
	var results = "";
	for(var i = 1; i < playersAndScores.length; i += 2)
	{
		results += "<tr><td class='score'>" + ((i + 1) / 2) + "</td><td class='score'>" + playersAndScores[i] + "</td><td class='score'>" + playersAndScores[i+1] + "</td></tr>";
	}

	//cahgne the board to say who won
	var table = document.getElementById('board');
	table.innerHTML = results;

	//get rid of the timer
	var timer = document.getElementById('timer');
	timer.innerHTML = "<p>Game Over!</p>";

	//so it doesnt flicker
	allShipsAreSunk = true;
}