<?php	
	include 'session.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Battleship</title>
	<script language="javascript" type="text/javascript" src="battleship-script.js"></script>
	<link rel="stylesheet" type="text/css" href="stylesheet.css" />
</head>

<body>

<h2> Start a Game of BattleShip!</h2>

Name: <input type="text" id="playerName" value="AnonymousScot">
Number of Players: <input type="number" id="desiredPlayers" min="2" max="7" value="2"><br><br>
<button id="startGame" onclick="createGame()">Create Game!</button>
<button id="joinGame" onclick="joinGame()">Join Game!</button>

<div id="statusdiv"></div>

</body>
</html>