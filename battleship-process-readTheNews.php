<?php
	include "session.php";

	//check if we started
	if($_SESSION["gameGoing"])
	{
		if($_POST["clientStarted"] == "false")
		{
			echo "fight";
		}
		else
		{
			$playerID = $_POST["id"];
			if($_SESSION["playersAlive"][$playerID] == TRUE)
			{
				//check the players ships
				$playersShips = $_SESSION["shipsLocation"];
				$shipsStatus = $_SESSION["shipsStatus"];
			
				$foundAShipLeft = FALSE;
				$hitString = "bn";
				for($i = 0; $i < sizeof($shipsStatus[$playerID]); $i++)
				{
					$foundNonHitSpot = false;
					for($j = 0; $j < sizeof($shipsStatus[$playerID][$i]); $j++)
					{
						if($shipsStatus[$playerID][$i][$j] == TRUE)
						{
							$hitString = $hitString." ".$playersShips[$playerID][$i][$j];
						}
						else
						{
							$foundNonHitSpot = TRUE;
							$foundAShipLeft = TRUE;
						}
					}
	
					if($foundNonHitSpot == TRUE)
					{
						//echo $hitString;
					}
					else
					{
						for($j = 0; $j < sizeof($playersShips[$playerID][$i]); $j++)
						{
							$playersShips[$playerID][$i][$j] = 9001;
						}
						$_SESSION["shipsLocation"] = $playersShips;
					}
				}
			
				if($foundAShipLeft == TRUE)
				{
					echo $hitString;
				}
				else
				{
					echo "taps";
					$playersAlive = $_SESSION["playersAlive"];
					$playersAlive[$playerID] = FALSE;
					$_SESSION["playersAlive"] = $playersAlive;
				}
			}
			else
			{
				echo "taps";

				//check to see if the game is over
				$playersAlive = $_SESSION["playersAlive"];

				$foundOneLiving = FALSE;
				$foundTwoLiving = FALSE;
				for($i = 0; $i < sizeof($playersAlive); $i++)
				{
					if($playersAlive[$i] == TRUE)
					{
						if($foundOneLiving == FALSE)
						{
							$foundOneLiving = TRUE;
						}
						else if($foundTWoLiving == FALSE)
						{
							$foundTwoLiving = TRUE;
						}
					}
				}
				if($foundTwoLiving == FALSE)
				{
					$_SESSION["gameGoing"] = FALSE;
				}
			}
		}
	}
	else if($_SESSION["gameInitiating"])
	{
		echo "wait";
	}
	else
	{
		echo "gg";	
		
		$playerNames = $_SESSION["players"];
		$playerScores = $_SESSION["scores"];
		for($j = 0; $j < sizeof($playerScores); $j++)
		{
			$highest = -1;
			$highestIndex = 0;
			for($i = 0; $i < sizeof($playerScores); $i++)
			{
				if($playerScores[$i] > $highest)
				{
					$highest = $playerScores[$i];
					$highestIndex = $i;
				}
			}
			echo " ".$playerNames[$highestIndex]." ".$playerScores[$highestIndex];
			$playerScores[$highestIndex] = -1;
		}				
	}
?>